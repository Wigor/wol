﻿using System.ComponentModel.DataAnnotations;

namespace Wigor.WOL.Web.Models;

public class Device
{
    [Required] public string IPAddress { get; set; }

    [Required] public string Mac { get; set; }

    [Required] public string Password { get; set; }
}