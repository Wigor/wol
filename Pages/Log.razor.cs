﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Wigor.WOL.Web.Models;

namespace Wigor.WOL.Web.Pages;

public partial class Log
{
    [Inject] HttpClient HttpClient { get; set; }
    private bool _isLoading = true;
    private DateTime? _date;
    private string _log;

    protected override async Task OnInitializedAsync()
    {
        await LoadingLog();
    }

    private async Task LoadingLog()
    {
        _isLoading = true;

        _log = await HttpClient.GetStringAsync($"/api/wol/log?year={_date?.Year}");

        _isLoading = false;
    }

    private async Task OnDateChange()
    {
        await LoadingLog();
    }
}