﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Wigor.WOL.Web.Models;
using Wigor.WOL.Web.Service;

namespace Wigor.WOL.Web.Controllers;

[ApiController]
[Route("api/[controller]")]
public class WOLController : ControllerBase
{
    private readonly WOLService _wolService;
    private readonly IConfiguration _configuration;

    public WOLController(WOLService wolService, IConfiguration configuration)
    {
        _wolService = wolService;
        _configuration = configuration;
    }

    [HttpPost("[action]")]
    public async Task<string> Wake([FromBody] Device device)
    {
        var pwd = _configuration.GetValue<string>("Pwd");
        await WriteLog($"请求日志，IP:{device.IPAddress}，Mac：{device.Mac}，PWD：{device.Password}");
        if (device.Password != $"{pwd}{DateTime.Now:yyyyMM}")
        {
            return "执行口令错误";
        }

        return _wolService.WOL(device.Mac);
    }

    [HttpGet("[action]")]
    public async Task<string> Log(int? year)
    {
        if (year is null or <= 1900)
        {
            year = DateTime.Now.Year;
        }

        var path = BuildLogPath(year.Value);
        if (!System.IO.File.Exists(path))
        {
            return string.Empty;
        }

        return await System.IO.File.ReadAllTextAsync(path);
    }

    private async Task WriteLog(string content)
    {
        var infoLogfile = BuildLogPath(DateTime.Now.Year);

        await using var sw = System.IO.File.AppendText(infoLogfile);
        await sw.WriteLineAsync("=================================");
        await sw.WriteLineAsync($"记录时间：{DateTime.Now:yyyy-MM-dd HH:mm:ss}");
        await sw.WriteLineAsync(content);
        await sw.WriteLineAsync("\r\n");
    }

    private string BuildLogPath(int year)
    {
        var root = Directory.GetCurrentDirectory();
        var filePath = $@"{root}\Logs\";
        if (Directory.Exists(filePath) == false) //如果不存在就创建file文件夹
        {
            Directory.CreateDirectory(filePath);
        }

        var infoLogfile = $"{filePath}log-{year}.txt";
        return infoLogfile;
    }
}